//Abdulwahed Ibrahim


import java.util.Scanner;

public class Program {

    public static void main(String args[]){
        Scanner myObj = new Scanner(System.in);  // Create a scanner for user input.
        System.out.println("Please enter an integer");
        String input = myObj.nextLine();

        int inputNum = Integer.parseInt(input);

        // I have called the a helper method to print the fibonaci sequence
        System.out.println("For loop:Fibonacci :" + inputNum);
        fibLoop(inputNum);

        // second use the recursion print out fibonacci
        System.out.println("Recursion:Fibonacci :" + inputNum);
        for (int i= 0; i < inputNum; i++)
        {
            System.out.println(fibRecursion(i));
        }

        //Second part of the task. printing factorials.
        // print factorial

        System.out.println("For loop:Factorial :" + inputNum);
        System.out.println(factorialLoop(inputNum)) ;
        System.out.println("Recursion:Factorial :" + inputNum);
        System.out.println( factorialRecursion(inputNum)) ;

        //print factorial using recursion

    }
    //create a helper method to use a for loop print fibonacci sequence
    static  void fibLoop(int n) {
        int n1 = 0;
        int n2 = 1;
        int total;
        System.out.println(n1);
        for (int i = 1; i < n; i++) {
            total = n1 + n2;
            System.out.println(n2);
            n1 = n2;
            n2 = total;
        }
    }
    //I have used recursion in this method to generate the fibonacci sequence.
    static int fibRecursion ( int n){

        if (n < 2) {
            return n;
        }
        return fibRecursion(n - 1) + fibRecursion(n - 2);
    }

    //created a helper method to generate a for loop to print factorials of a value
     static int factorialLoop(int value) {
        int f = 1;
        for (int i = 2; i <= value; i++) {
            f *= i;
        }
        return f;
    }

    //created a helper method to generate a recursion to print factorials of a value
    static int factorialRecursion(int value) {
        if (value <= 2) {
            return value;
        }
        return value * factorialLoop(value - 1);
    }
}
